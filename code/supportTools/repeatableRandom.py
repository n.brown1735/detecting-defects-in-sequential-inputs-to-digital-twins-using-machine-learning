"""
repeatableRandom.py

Notes:
    The purpose of this function is to produce a repeatable random number stream
    across the entire project that is system and architecture agnostic. NumPy
    provides such a pseudo random number generator, but it is not guaranteed to
    be repeatable across NumPy versions. For this reason, Numpy will be forced
    to a specific version
  
    It is important to realize that changing certain portions of code that call
    this function may produce different results going forward.

Use:
    repRand()
  
    Returns a random number between 0 and 1
  
"""

from pkg_resources import require
require("numpy==1.24.3")  # modified to use specific numpy
from numpy.random import default_rng

# Set the random number seed and do not change it to keep results repeatable
seed = 0

# Create random number generator with seed
rg = default_rng(seed)

# Define the functions, so that it can be used elsewhere
def repRand():
    return rg.random()

def repRandInt(low, high):
    return rg.integers(low, high, endpoint=True)

def repRandDec(low, high):
    return rg.random() * (high - low) + low

def choice(count=1, size=1, replace=False):
    return rg.choice(count, size, replace)
    

# Reset the generator
def repRandReset(seed=0):
    global rg
    del rg
    rg = default_rng(seed)

