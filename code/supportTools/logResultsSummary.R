# """
# logResultsSummary.R
# 
# Notes:
#     The purpose of this module is to log and calculate the summary statistics
#     from the defect detection results. The summary is saved to a CSV file.
# 
# Use:
#     logResultsSummary(df, technique, column)
#         
#         Arguments:
#         df        -- Dataframe containing defect data
#         technique -- Name of the technique used
#         column    -- Column to evaluate summary (ex. "defectDetectionName")
# 
#     Saves a CSV with results summary
#
#     logResults(df)
#
#         Arguments:
#         df        -- Dataframe containing defect data
#
#     Does all the logging for all the techniques so far, manually updated
# 
# """



logResultsSummary <- function(df, Technique, column) {
    # Big TODO here, need to add a parameter to other files to pass this in. Just
    # use it as a switch for now.
    location <- "/data"
    #location <- "/dataTest"
    
    # Read current file, or create it if it does not exist
    if (file.exists(paste(getwd(), location, "/resultsSummary.csv", sep=""))) {
        csv <- read.csv(paste(getwd(), location, "/resultsSummary.csv", sep=""))
    } else {
        csv <- list("Technique", "TrueNegative", "TruePositiveLow", 
                    "TruePositiveMed", "TruePositiveHigh", "FalsePositive", 
                    "FalseNegativeLow", "FalseNegativeMed", "FalseNegativeHigh", 
                    "Accuracy", "Specificity",
                    "Recall", "Recall_L", "Recall_U", "Precision", "Precision_L", "Precision_U",
                    "NPV", "F1", "F1_L", "F1_U", "F0.5", "F2")
        write.table(csv, paste(getwd(), location, "/resultsSummary.csv", sep=""), sep=",", col.names=FALSE, row.names=FALSE)
        csv <- read.csv(paste(getwd(), location, "/resultsSummary.csv", sep=""))
    }
    
    # Check if current technique already exists. If so, remove it.
    csv <- csv[csv$Technique != Technique, ]
    
    
    # Calculate the results of defect detection
    TrueNegative <- nrow(df[which(df$rowDefect==0 & df[,column]==0), ])
    TruePositiveLow <- nrow(df[which(df$rowDefect==1 & df[,column]==1 & df$defectSeverity=="Low"), ])
    TruePositiveMed <- nrow(df[which(df$rowDefect==1 & df[,column]==1 & df$defectSeverity=="Med"), ])
    TruePositiveHigh <- nrow(df[which(df$rowDefect==1 & df[,column]==1 & df$defectSeverity=="High"), ])
    FalsePositive <- nrow(df[which(df$rowDefect==0 & df[,column]==1), ])
    FalseNegativeLow <- nrow(df[which(df$rowDefect==1 & df[,column]==0 & df$defectSeverity=="Low"), ])
    FalseNegativeMed <- nrow(df[which(df$rowDefect==1 & df[,column]==0 & df$defectSeverity=="Med"), ])
    FalseNegativeHigh <- nrow(df[which(df$rowDefect==1 & df[,column]==0 & df$defectSeverity=="High"), ])
    
    # Group results into false/true positive/negative
    TruePositive  <- TruePositiveLow + TruePositiveMed + TruePositiveHigh
    FalseNegative <- FalseNegativeLow + FalseNegativeMed + FalseNegativeHigh
    
    # Calculate metrics
    Accuracy    <- (TruePositive + TrueNegative) / 
                   (TruePositive + TrueNegative + FalsePositive + FalseNegative)
    
    Specificity <- TrueNegative / (TrueNegative + FalsePositive)
    
    Recall      <- TruePositive / (TruePositive + FalseNegative)
    
    Precision   <- TruePositive / (TruePositive + FalsePositive)
    
    NPV         <- TrueNegative / (TrueNegative + FalseNegative)
    
    # Setup ThresholdROC
    library(ThresholdROC)
    tab <- as.table(matrix(c(TruePositive, FalsePositive, FalseNegative, TrueNegative), ncol=2, byrow=TRUE))
    result <- tryCatch({result <- diagnostic(tab)}, error=function(e){result <- as.table(0)})
    
    # If able, calculate the lower and upper bounds
    if (dim(result)[1] == 1) {
        RecallL <- NA
        RecallU <- NA
        PrecL  <- NA
        PrecU  <- NA
    } else {
        RecallL <- result["Sensitivity","Low.lim(95%)"]
        RecallU <- result["Sensitivity","Up.lim(95%)"]
        PrecL  <- result["Pos.Pred.Val.","Low.lim(95%)"]
        PrecU  <- result["Pos.Pred.Val.","Up.lim(95%)"]
    }
    
    Fscore_beta <- 1
    F1 <- (1+Fscore_beta^2)*TruePositive / 
              ((1+Fscore_beta^2)*TruePositive + Fscore_beta^2*FalseNegative + FalsePositive)
    F1L <- NA #upper and lower bounds found in FmeasureBootstrapping.R manually for now
    F1U <- NA
    
    Fscore_beta <- 0.5
    Fhalf <- (1+Fscore_beta^2)*TruePositive / 
        ((1+Fscore_beta^2)*TruePositive + Fscore_beta^2*FalseNegative + FalsePositive)
    
    Fscore_beta <- 2
    F2 <- (1+Fscore_beta^2)*TruePositive / 
        ((1+Fscore_beta^2)*TruePositive + Fscore_beta^2*FalseNegative + FalsePositive)
    
    # Add data to a new row
    csv[nrow(csv)+1,] <- list(Technique, TrueNegative, TruePositiveLow, 
                              TruePositiveMed, TruePositiveHigh, FalsePositive, 
                              FalseNegativeLow, FalseNegativeMed, FalseNegativeHigh, 
                              Accuracy, Specificity,
                              Recall, RecallL, RecallU, Precision, PrecL, PrecU,
                              NPV, F1, F1L, F1U, Fhalf, F2)
    
    # Save results
    write.csv(csv, paste(getwd(), location, "/resultsSummary.csv", sep=""), row.names=FALSE)
}


logResults <- function(df) {
    # Manually updated. Just a quick function to calculate success measure without
    # needing to run the entire model again, which takes a while.
    logResultsSummary(df,"Baseline - Always Defect","baselineDefectAll")
    logResultsSummary(df,"Baseline - Never Defect","baselineDefectNone")
    logResultsSummary(df,"Baseline - Random","baselineDefectRandom")
    logResultsSummary(df,"Second-Order Polynomial Regression - 80% CI","polyDefect_0.8")
    logResultsSummary(df,"Second-Order Polynomial Regression - 90% CI","polyDefect_0.9")
    logResultsSummary(df,"Second-Order Polynomial Regression - 95% CI","polyDefect_0.95")
    logResultsSummary(df,"Second-Order Polynomial Regression - 99% CI","polyDefect_0.99")
    logResultsSummary(df,"Second-Order Polynomial Regression - 99.9% CI","polyDefect_0.999")
    logResultsSummary(df,"Second-Order Polynomial Regression - 99.92% CI","polyDefect_0.9992")
    logResultsSummary(df,"Second-Order Polynomial Regression - 99.94% CI","polyDefect_0.9994")
    logResultsSummary(df,"Second-Order Polynomial Regression - 99.96% CI","polyDefect_0.9996")
    logResultsSummary(df,"Second-Order Polynomial Regression - 99.98% CI","polyDefect_0.9998")
    logResultsSummary(df,"Second-Order Polynomial Regression - 99.999% CI","polyDefect_0.99999")
    logResultsSummary(df,"Second-Order Polynomial Regression - 99.9999% CI","polyDefect_0.999999")
    logResultsSummary(df,"Second-Order Polynomial Regression - 99.99999% CI","polyDefect_0.9999999")
    logResultsSummary(df,"Second-Order Polynomial Regression - 99.999999% CI","polyDefect_0.99999999")
}
