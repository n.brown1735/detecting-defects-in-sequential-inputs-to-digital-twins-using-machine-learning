"""
helperClasses.py

Notes:
    This file just contains various useful classes and functions that do not
    necessarily belong to other areas.
    
"""



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Dot Notation
Purpose: Dot notation access to dictionary attributes

Example:
    mydict = {'val':'it works'}
    mydict = dotdict(mydict)
    mydict.val
    # 'it works'
    
"""
class dotdict(dict):
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Log Defect Detection Results
Purpose: Log results of the various defect detection techniques to a
         standardized data structure
         
Example:
    logResults(df, technique, defectDetCol)
    
    Arguments:
        df           -- DataFrame object containing flight data
        technique    -- String with description about the technique used
        defectDetCol -- Column containing 1 or 0 indicating defect detected
        
"""

def logResults(df, technique, defectDetCol):
    print("nothing for now, might do in R")
    
    
    
