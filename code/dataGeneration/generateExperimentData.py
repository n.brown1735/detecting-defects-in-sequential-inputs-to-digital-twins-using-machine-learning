"""
generateData.py

Notes:
    The purpose of this function is to modify the control data for the training
    and testing of defect detection models. This includes randomly inserting
    defects, but does not include 

Use:
    modifyData(data, expProp, modProp)
        
        Arguments:
        data    -- Pandas DataFrame object containing flight data
        expProp -- Proportion between 0 and 1 of experimental trajectories versus
                   control, results round(quant*expProp) experimentals (default 0)
        modProp -- Proportion between 0 and 1 of data in an experimental
                   trajectory that is modified, should be small (default 0)

    Returns data structure with control and experimental trajectories

"""

from repeatableRandom import *
from tqdm import tqdm
from math import sqrt, log, cos, pi
from collections import Counter
from pandas import unique

# Start by defining a few strategies for modifying the data
# Gaussian (normal) distribution
def gauss(stdev=1):
    return stdev * sqrt(-2 * log(repRand()) ) * cos(2 * pi * repRand())

# Uniform distribution
def unif(spread=1):
    return repRandDec(-1 * spread, spread)

# Randomly modify some of the control data to be experimental data
def modifyData(data, expProp=0, modProp=0):
    # Validate arguments
    if not (0 <= expProp <= 1):
        ValueError('Proportion of experimental flights should be between 0 and 1')
    
    if not (0 <= modProp <= 1):
        ValueError('Proportion of modified flight data should be between 0 and 1')
    
    # Reset repeatable RNG
    repRandReset()
    
    # Determine number of experimental and control trajectories
    quant = len( unique(data.flightID) )
    exp = round( quant * expProp )
    ctrl = quant - exp
    print('Out of ' + str(quant) + ' total entries, there will be ' +
          str(ctrl) + ' control data and ' + str(exp) + ' experimental data. \n')
    
    # Determine which flights will be experimental
    expFlights = [( 'Flight' + str(x).rjust(5, '0') ) 
                  for x in (choice(quant, exp, False) + 1)]
    
    # Loop through experimental data and modify specific data points
    for flight in tqdm(expFlights):
        # Set flightDefect to 1 for this experimental flightID
        data.loc[data.flightID == flight, 'flightDefect'] = 1
        
        # Determine which rows will be experimental
        numRows = len( data.loc[data.flightID == flight] )
        numExpRows = max( 1, round(numRows*modProp) ) # Always select at least 1
        expRows = choice(numRows, numExpRows, False)
        minIndex = min(data.loc[data.flightID == flight].index) # Need for adding to expRows
        
        # Loop through expRows and modify data per described techniques
        for row in expRows:
            # First, set the defect row flag
            data.iloc[minIndex + row, data.columns.get_loc("rowDefect")] = 1
            
            # Then set some defects
            data.iloc[minIndex + row, data.columns.get_loc("x")]  += gauss(0.75)
            data.iloc[minIndex + row, data.columns.get_loc("y")]  += gauss(0.75)
            data.iloc[minIndex + row, data.columns.get_loc("z")]  += gauss(0.75)
            #data.iloc[minIndex + row, data.columns.get_loc("vx")] += gauss(100)
            #data.iloc[minIndex + row, data.columns.get_loc("vy")] += gauss(100)
            #data.iloc[minIndex + row, data.columns.get_loc("vz")] += gauss(100)
            #TODO probably will need to tune the gauss() value to give even number of low/med/high
            
        
        
    return data
