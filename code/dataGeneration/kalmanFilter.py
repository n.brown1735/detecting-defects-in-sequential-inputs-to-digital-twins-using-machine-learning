"""
kalmanFilter.py

Notes:
    The purpose of this module is to use a Kalman filter for motion to smooth
    the state updates and predict the next state and covariance. This will be
    used to assign low, medium, and high severity to defects.

Use:
    See each function below

"""

from numpy import array, eye, linalg

# Function to initialize Kalman filter
def initKalman():
    '''
        The purpose of this function is to initialize the Kalman filter. The
        initial state (x) is assumed to be zeros because all flight data uses
        the origin as the starting point and the initial covariance (P) is
        assumed to be quite small at first due to our knowledge of the flight
        starting point and initial velocity.
        
        Outputs:
            x       -- Initial assumed state [x, vx, ax, y, vy, ay, z, vz, az]
            P       -- Initial assumed estimate uncertainty
    '''
    x = array([[   0,    0,    0,    0,    0,    0,    0,    0,    0]]).T
    P = array([[100000,    0,    0,    0,    0,    0,    0,    0,    0],
               [   0, 100000,    0,    0,    0,    0,    0,    0,    0],
               [   0,    0, 100000,    0,    0,    0,    0,    0,    0],
               [   0,    0,    0, 1000,    0,    0,    0,    0,    0],
               [   0,    0,    0,    0, 1000,    0,    0,    0,    0],
               [   0,    0,    0,    0,    0, 1000,    0,    0,    0],
               [   0,    0,    0,    0,    0,    0, 100,    0,    0],
               [   0,    0,    0,    0,    0,    0,    0, 100,    0],
               [   0,    0,    0,    0,    0,    0,    0,    0, 100]])
                
    return x, P
    
    
# Function to step through measurements and update the Kalman filter
def stepKalman(x, P, deltaT, sigma_a, sigma_m, z):
    '''
        Inputs:
            x       -- Current state
            P       -- Current estimate uncertainty
            deltaT  -- Time difference since current state
            sigma_a -- Acceleration st dev
            sigma_m -- Measurement st dev
            z       -- Measurement
            
        Outputs:
            x       -- Next state
            P       -- Next estimate uncertainty
            
    '''
    
    # State transition matrix
    F = array([[1, deltaT, 0.5*(deltaT**2), 0,      0,               0, 0,      0,               0],
               [0,      1,          deltaT, 0,      0,               0, 0,      0,               0],
               [0,      0,               1, 0,      0,               0, 0,      0,               0],
               [0,      0,               0, 1, deltaT, 0.5*(deltaT**2), 0,      0,               0],
               [0,      0,               0, 0,      1,          deltaT, 0,      0,               0],
               [0,      0,               0, 0,      0,               1, 0,      0,               0],
               [0,      0,               0, 0,      0,               0, 1, deltaT, 0.5*(deltaT**2)],
               [0,      0,               0, 0,      0,               0, 0,      1,          deltaT],
               [0,      0,               0, 0,      0,               0, 0,      0,               1]])
                
    # Process noise matrix
    Q = array([[(deltaT**4)/4, (deltaT**3)/2, (deltaT**2)/2, 0, 0, 0, 0, 0, 0],
               [(deltaT**3)/2,     deltaT**2,        deltaT, 0, 0, 0, 0, 0, 0],
               [(deltaT**2)/2,        deltaT,             1, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, (deltaT**4)/4, (deltaT**3)/2, (deltaT**2)/2, 0, 0, 0],
               [0, 0, 0, (deltaT**3)/2,     deltaT**2,        deltaT, 0, 0, 0],
               [0, 0, 0, (deltaT**2)/2,        deltaT,             1, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, (deltaT**4)/4, (deltaT**3)/2, (deltaT**2)/2],
               [0, 0, 0, 0, 0, 0, (deltaT**3)/2,     deltaT**2,        deltaT],
               [0, 0, 0, 0, 0, 0, (deltaT**2)/2,        deltaT,             1]])
    Q = Q * sigma_a**2
    
    # Observation matrix
    H = array([[1, 0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 1, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 1, 0, 0]])
                
    # Measurement covariance (uncertainty) matrix
    R = array([[sigma_m**2,          0,          0],
               [         0, sigma_m**2,          0],
               [         0,          0, sigma_m**2]])
                
    # Identity matrix the size and shape of F
    I = array(eye(F.shape[0]))
    
    # Predict the next state and covariance
    x = F @ x
    P = F @ P @ F.T + Q
    
    # Kalman gain
    K = P @ H.T @ linalg.inv(H @ P @ H.T + R)

    # State update
    x = x + K @ (z - H @ x)
    
    # Covariance update
    P = (I - K @ H) @ P @ (I - K @ H).T + K @ R @ K.T
    
    return x, P
    


# Function to predict the next step based on current Kalman state without updates
def predictKalman(x, P, deltaT, sigma_a):
    '''
        Inputs:
            x       -- Current state
            P       -- Current estimate uncertainty
            deltaT  -- Time difference since current state
            sigma_a -- Acceleration st dev
            
        Outputs:
            predx   -- Predicted next state
            predP   -- Predicted next estimate uncertainty
            
    '''
    
    # State transition matrix
    F = array([[1, deltaT, 0.5*(deltaT**2), 0,      0,               0, 0,      0,               0],
               [0,      1,          deltaT, 0,      0,               0, 0,      0,               0],
               [0,      0,               1, 0,      0,               0, 0,      0,               0],
               [0,      0,               0, 1, deltaT, 0.5*(deltaT**2), 0,      0,               0],
               [0,      0,               0, 0,      1,          deltaT, 0,      0,               0],
               [0,      0,               0, 0,      0,               1, 0,      0,               0],
               [0,      0,               0, 0,      0,               0, 1, deltaT, 0.5*(deltaT**2)],
               [0,      0,               0, 0,      0,               0, 0,      1,          deltaT],
               [0,      0,               0, 0,      0,               0, 0,      0,               1]])
                
    # Process noise matrix
    Q = array([[(deltaT**4)/4, (deltaT**3)/2, (deltaT**2)/2, 0, 0, 0, 0, 0, 0],
               [(deltaT**3)/2,     deltaT**2,        deltaT, 0, 0, 0, 0, 0, 0],
               [(deltaT**2)/2,        deltaT,             1, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, (deltaT**4)/4, (deltaT**3)/2, (deltaT**2)/2, 0, 0, 0],
               [0, 0, 0, (deltaT**3)/2,     deltaT**2,        deltaT, 0, 0, 0],
               [0, 0, 0, (deltaT**2)/2,        deltaT,             1, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, (deltaT**4)/4, (deltaT**3)/2, (deltaT**2)/2],
               [0, 0, 0, 0, 0, 0, (deltaT**3)/2,     deltaT**2,        deltaT],
               [0, 0, 0, 0, 0, 0, (deltaT**2)/2,        deltaT,             1]])
    Q = Q * sigma_a**2
    
    # Predict next state and covariance
    predx = F @ x
    predP = F @ P @ F.T + Q
    
    return predx, predP

