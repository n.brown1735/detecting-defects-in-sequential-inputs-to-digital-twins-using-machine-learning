"""
defectSeverity.py

Notes:
    The purpose of this module is to assign a severity to the defective data. We
    will use a Kalman Filter, which is descibed in the paper.
    
    Defect Severity
    Low  -> Most elements in a simulation will not behave poorly with defect
    Med  -> Some elements will behave poorly with medium defects
    High -> Most or all elements will behave poorly with high defects

Use:
    defectSeverity(data, sigma_m)
        
        Arguments:
        data    -- Pandas DataFrame object containing flight data
        sigma_m -- Measurement st dev for Kalman filter, but used as a measure of model sensitivity

    Returns data structure with control and experimental trajectories adding
    the defect severity for all defects

"""

from kalmanFilter import *
from tqdm import tqdm
from numpy import array, sqrt

def defectSeverity(data, sigma_m):
    # Get list of flights in data
    flights = data.flightID.unique().tolist()
    
    # Loop through flights and pass through Kalman filter
    for flight in tqdm(flights):
        # Determine flight information to loop through
        numRows = len( data.loc[data.flightID == flight] )
        minIndex = min(data.loc[data.flightID == flight].index) # Need for adding to row
        
        # Initialize Kalman filter
        x, P = initKalman()
        data.iloc[minIndex, data.columns.get_loc("xKalman")] = x[0,0]
        data.iloc[minIndex, data.columns.get_loc("yKalman")] = x[3,0]
        data.iloc[minIndex, data.columns.get_loc("zKalman")] = x[6,0]
        data.iloc[minIndex, data.columns.get_loc("xKalVar")] = P[0,0]
        data.iloc[minIndex, data.columns.get_loc("yKalVar")] = P[1,1]
        data.iloc[minIndex, data.columns.get_loc("zKalVar")] = P[2,2]
        sigma_a=0.2
        
        # Loop through rows and include Kalman filter predictions
        for row in range(1, numRows):
            # Get measurement
            z = array([[data.iloc[minIndex + row, data.columns.get_loc("x")]],
                       [data.iloc[minIndex + row, data.columns.get_loc("y")]],
                       [data.iloc[minIndex + row, data.columns.get_loc("z")]]])
            
            # Get timestep
            deltaT = data.iloc[minIndex + row, data.columns.get_loc("time")] - \
                     data.iloc[minIndex + row - 1, data.columns.get_loc("time")]
            
            # Step Kalman filter and store in data
            x, P = stepKalman(x, P, deltaT, sigma_a, sigma_m, z)
            data.iloc[minIndex + row, data.columns.get_loc("xKalman")] = x[0,0]
            data.iloc[minIndex + row, data.columns.get_loc("yKalman")] = x[3,0]
            data.iloc[minIndex + row, data.columns.get_loc("zKalman")] = x[6,0]
            data.iloc[minIndex + row, data.columns.get_loc("xKalVar")] = P[0,0]
            data.iloc[minIndex + row, data.columns.get_loc("yKalVar")] = P[1,1]
            data.iloc[minIndex + row, data.columns.get_loc("zKalVar")] = P[2,2]
            
    
    # Assign severity to each row that is flagged as modified
    columnList = ['x','y','z'] # List of columns we care about with defects
    
    data['defectSeverity'] = 'None' # Initialize all defectSeverity values
    data.loc[data.rowDefect==1, 'defectSeverity'] = 'Low' # All defects are at least low
    
    
    for column in data.loc[:,columnList].columns:
        kalVarCol = column + 'KalVar'
        cleanCol = column + 'Clean'
        
        # Medium defects are at least one standard deviation from expectation (Kalman)
        data.loc[(data.rowDefect==1) & (data.defectSeverity != "High") & (abs(data[column]-data[cleanCol])>sqrt(data[kalVarCol])), 'defectSeverity'] = 'Med'
        
        # High defects are at least two standard deviations from expectation (Kalman)
        data.loc[(data.rowDefect==1) & (abs(data[column]-data[cleanCol])>2*sqrt(data[kalVarCol])), 'defectSeverity'] = 'High'

    
    return data
