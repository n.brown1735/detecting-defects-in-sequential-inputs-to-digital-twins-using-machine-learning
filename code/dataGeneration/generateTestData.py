"""
generateTestData.py

Notes:
    The purpose of this module is to create test data to run defect detection
    models against, which is beneficial to testing against overfitting

Use:
    generateTestData()
  
    Returns a full data set similar to the original set in /data.
  
"""


def generateTestData():
    # Set the random number seed and do not change it to keep results repeatable
    # Note that the training data is created with seed=0, choose something else
    seedTest = 1
    
    # Generate baseline data
    from generateData import generateData
    dataTest = generateData(50, seedTest)
    
    # Introduce defects
    from generateExperimentData import modifyData
    dataTest = modifyData(dataTest, expProp=0.667, modProp=0.01)
    
    # Measure severity of defects using Kalman filter
    from defectSeverity import defectSeverity
    dataTest = defectSeverity(dataTest, sigma_m=1)
    
    return dataTest
