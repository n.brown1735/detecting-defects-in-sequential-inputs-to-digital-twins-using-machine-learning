"""
generateTrajectory.py

Notes:
    The purpose of this function is to generate a trajectory based on a set of
    inputs defined below. Rocketpy is the generator of the data.

Use:
    generateTrajectory(lat, long, inc, heading, burnout)
  
        Arguments:
        lat     -- Latitude degrees in decimal format [-90, 90] (default 0)
        long    -- Longitude degrees in decimal format [-180, 180] (default 0)
        inc     -- Inclination degrees relative to ground [45, 85] (default 85)
        heading -- Heading degrees relative to North [0, 360] (default 0)
        burnout -- Time to burnout of fuel in seconds [4, 45] (default 4)
  
    Returns a dictionary object 
  
"""

from rocketpy import Environment, Rocket, SolidMotor, Flight
import sys

def generateTrajectory(lat=0, long=0, inc=85, heading=0, burnout=4):
    
    # Validate arguments
    if not (-90 <= lat <= 90):
        ValueError('Latitude must be between -90 and 90 degrees')
    
    if not (-180 <= long <= 180):
        ValueError('Longitude must be between -180 and 180 degrees')
        
    if not (45 <= inc <= 85):
        ValueError('Inclination must be between 45 and 90 degrees relative to ground')
        
    if not (0 <= heading <= 360):
        ValueError('Heading must be between 0 and 360 degrees relative to North')
        
    if not (4 <= burnout <= 45):
        ValueError('Burnout must be between 4 and 45 seconds')
        
        
    # Begin setup of Flight

    Env = Environment(
        railLength=5.2,
        latitude=lat,
        longitude=long,
        elevation=0,
        date=(2023, 1, 22, 12) # May as well fix the date
    ) 
    
    Env.setAtmosphericModel(type='StandardAtmosphere', file='GFS')
    
    Pro75M1670 = SolidMotor(
        thrustSource=1000, #"./my_env/Lib/data/motors/Cesaroni_M1670.eng",
        burnOut=burnout,
        grainNumber=5,
        grainSeparation=5/1000,
        grainDensity=1815,
        grainOuterRadius=33/1000,
        grainInitialInnerRadius=15/1000,
        grainInitialHeight=120/1000,
        nozzleRadius=33/1000,
        throatRadius=11/1000,
        interpolationMethod='linear'
    )
    
    Calisto = Rocket(
        motor=Pro75M1670,
        radius=127/2000,
        mass=19.197-2.956,
        inertiaI=6.60,
        inertiaZ=0.0351,
        distanceRocketNozzle=-1.255,
        distanceRocketPropellant=-0.85704,
        powerOffDrag=sys.path[len(sys.path)-1]+"../../rocketPyData/calisto/powerOffDragCurve.csv",
        powerOnDrag=sys.path[len(sys.path)-1]+"../../rocketPyData/calisto/powerOnDragCurve.csv"
    )
    
    Calisto.setRailButtons([0.2, -0.5])
    
    NoseCone = Calisto.addNose(length=0.55829, kind="vonKarman", distanceToCM=0.71971)
    
    FinSet = Calisto.addFins(4, span=0.100, rootChord=0.120, tipChord=0.040, distanceToCM=-1.04956)
    
    Tail = Calisto.addTail(topRadius=0.0635, bottomRadius=0.0435, length=0.060, distanceToCM=-1.194656)
    
    # Fly rocket using parameters above
    outputFlight = Flight(rocket=Calisto, environment=Env, inclination=inc, heading=heading)
    
    return outputFlight
