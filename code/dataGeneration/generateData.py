"""
generateData.py

Notes:
    The purpose of this function is to generate all data for the training and
    testing of defect detection models.

Use:
    generateData(quant)
    
        Arguments:
        quant   -- the number of trajectories desired (default 1)

    Returns data structure with control trajectories

"""

from generateTrajectory import generateTrajectory
from repeatableRandom import *
from tqdm import tqdm
from math import floor
from numpy import arange
from pandas import DataFrame


# Generate trajectories and prune data to reduce total size
def generateData(quant=1, seed=0):
    # Validate inputs
    if not (1 <= quant):
        ValueError('The number of flights should be greater than or equal to 1')
        
    # Reset random number generator
    repRandReset(seed)
    
    # Initialize data structure
    data = {
        "flightID": [],         #Unique ID for each flight
        "flightDefect": [],     #Flag to mark a flight with at least one defect
        "rowDefect": [],        #Flag to mark a row with at least one defect
        "defectSeverity": [],   #Severity of the defect, if there is one
        "time": [],             #Flight time (s)
        "x": [],                #x-axis position (m), may contain defect
        "xClean": [],           #x-axis (m), value before defect introduced
        "xKalman": [],          #x-axis (m), predicted from Kalman filter
        "xKalVar": [],          #x-axis variance of prediction from Kalman filter
        "y": [],                #y-axis position (m), may contain defect
        "yClean": [],           #y-axis (m), value before defect introduced
        "yKalman": [],          #y-axis (m), predicted from Kalman filter
        "yKalVar": [],          #y-axis variance of prediction from Kalman filter
        "z": [],                #z-axis position (m), may contain defect
        "zClean": [],           #z-axis (m), value before defect introduced
        "zKalman": [],          #z-axis (m), predicted from Kalman filter
        "zKalVar": [],          #z-axis variance of prediction from Kalman filter
        "vx": [],               #x-axis velocity (m/s), may contain defect
        "vxClean": [],          #x-axis velocity (m/s), value before defect introduced
        "vy": [],               #y-axis velocity (m/s), may contain defect
        "vyClean": [],          #y-axis velocity (m/s), value before defect introduced
        "vz": [],               #z-axis velocity (m/s), may contain defect
        "vzClean": [],          #z-axis velocity (m/s), value before defect introduced
        "ax": [],               #x-axis acceleration (m/s^2)
        "ay": [],               #y-axis acceleration (m/s^2)
        "az": []                #z-axis acceleration (m/s^2)
    }
    
    # Generate the trajectories and clean the data to be more manageable
    for i in tqdm(range(quant)):
        # Random inputs for flight
        lat = repRandDec(-90, 90)
        long = repRandDec(-180, 180)
        inc = repRandDec(45, 85)
        heading = repRandDec(0, 360)
        burnout = repRandDec(4, 45)

        # Generate flight
        flight = generateTrajectory(lat, long, inc, heading, burnout)
        
        # Convert flight object into data structure
        flightName = 'Flight' + str(i+1).rjust(5, '0')
        timeIncrements = list(arange(0, floor(flight.t*4)/4+0.001, 0.25))
        timeIncrements.append(round(flight.t, 2)) #include final time and state of flight
        
        flight.postProcess()
        data["flightID"].extend([flightName] * len(timeIncrements))
        # Flight and data defects and detections will be set to something != 0 later
        data["flightDefect"].extend([0] * len(timeIncrements))
        data["rowDefect"].extend([0] * len(timeIncrements))
        data["defectSeverity"].extend(['None'] * len(timeIncrements))
        data["time"].extend(timeIncrements)
        data["x"].extend(flight.x(timeIncrements))
        data["xClean"].extend(flight.x(timeIncrements))
        data["xKalman"].extend([0] * len(timeIncrements))
        data["xKalVar"].extend([0] * len(timeIncrements))
        data["y"].extend(flight.y(timeIncrements))
        data["yClean"].extend(flight.y(timeIncrements))
        data["yKalman"].extend([0] * len(timeIncrements))
        data["yKalVar"].extend([0] * len(timeIncrements))
        data["z"].extend(flight.z(timeIncrements))
        data["zClean"].extend(flight.z(timeIncrements))
        data["zKalman"].extend([0] * len(timeIncrements))
        data["zKalVar"].extend([0] * len(timeIncrements))
        data["vx"].extend(flight.vx(timeIncrements))
        data["vxClean"].extend(flight.vx(timeIncrements))
        data["vy"].extend(flight.vy(timeIncrements))
        data["vyClean"].extend(flight.vy(timeIncrements))
        data["vz"].extend(flight.vz(timeIncrements))
        data["vzClean"].extend(flight.vz(timeIncrements))
        data["ax"].extend(flight.ax(timeIncrements))
        data["ay"].extend(flight.ay(timeIncrements))
        data["az"].extend(flight.az(timeIncrements))
        
    
    # Convert to pandas DataFrame
    data = DataFrame(data)
    return data
    
