This project contains both Python and R code. The intent is that you will follow the steps in Main.py first, which will eventually instruct you to either run Main.R in one shot or navigate over to Main.R and step through it.

Note: Please follow the steps in Rsetup.txt and PYsetup.txt to initialize your work area.