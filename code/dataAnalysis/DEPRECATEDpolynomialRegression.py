"""
polynomialRegression.py

Notes:
    The purpose of this module is to detect defects in the data using a
    polynomial regression model. The errors from the model will determine what
    is within the range of reasonability

Use:
    polynomialRegression(data)
        
        Arguments:
        data    -- Pandas DataFrame object containing flight data

    Returns data structure with control and experimental trajectories

"""

def polynomialRegression(data):
    from tqdm import tqdm
    from numpy import poly1d, polyfit

    # TODO validate the dataframe for use in this module
    
    # TODO Pass this dataframe to R (another file?) 
    
    # Set the window for polynomial regression (value is past trajectory updates)
    N = 10
    
    # Instantiate new columns for regression results
    data['stuff']='default'
    
    # Loop through each row, breaking regression models by flight and time
    for i in tqdm(data.index):
        # Determine flight information and timestamp
        flight = data.flightID[i]
        thisTime = data.time[i]
        
        # Check if there are at least N, if not move on to next iteration
        temp = data.loc[(data.flightID == flight) & (data.time < thisTime), 'time'].tail(N)
        if len(temp) < 10:
            continue
        
        # Collect past N data points to model current state data
        tempData = data.loc[(data.flightID == flight) & (data.time < thisTime), ['time','x', 'y', 'z']].tail(N)
        
        # Build polynomial regression models
        polyX = poly1d(polyfit(tempData.time, tempData.x, 3))
        polyY = poly1d(polyfit(tempData.time, tempData.y, 3))
        polyZ = poly1d(polyfit(tempData.time, tempData.z, 3))
        
    # Clean up memory
    del temp, tempData
        
