"""
Main.py

Notes:
    The purpose of the Main module is to generate all data, clean it, modify the
    experimental data, execute tests in R to detect defects, and graph results.

Use:
    Run each line step by step
    
IMPORTANT:
    I have designed this file to be run one or two lines at a time. Running
    everything at once may be ill-advised. Certainly you will not need to
    generate the data each time you run the file and just read the generated
    data.
  
"""

##### Debug Steps #####
# import traceback
# import logging
#try:
#    <whatever line you want to debug>
#except Exception:
#    logging.error(traceback.format_exc())


##### Set Path #####
# Must be set by user to top level of project
source="C:/your/path"


##### Add source code to path #####
import sys
source=source.rstrip('//') # Remove final / if it exists
sys.path.append(source + "/code/dataGeneration/")
sys.path.append(source + "/code/dataSummaryAndPlots/")
sys.path.append(source + "/code/examplesAndTests/")
sys.path.append(source + "/code/dataAnalysis/")
sys.path.append(source + "/code/supportTools/")


##### Generate Data #####
# NOTE: Skip to Read Data section, if already generated

# Generate baseline data
from generateData import generateData
data = generateData(quant=50)

# Introduce defects
from generateExperimentData import modifyData
data = modifyData(data, expProp=0.667, modProp=0.01)

# Measure severity of defects using Kalman filter
from defectSeverity import defectSeverity
data = defectSeverity(data, sigma_m=1)


##### Save Data #####
# Saving as a CSV file for easy manual validation
data.to_csv(source + "/data/data.csv", index=False)


##### Read Data #####
from pandas import read_csv
data = read_csv(source + "/data/data.csv", index_col=False)


##### Exploratory data summaries and plots #####

# Show Kalman filter predictions and error bounds
from kalmanFilterPlots import *

# Summary of base data
from baseDataStatistics import baseDataStatistics
baseDataStatistics(data, source + "/data/")

# Base data plots
from baseDataPlots import *
defectExamplePlots(source + "/plots/")
flightPlots(data, 'Flight00001', source + "/plots/")


##### Data Analysis #####
# At this point, we let R do the heavy statistical lifting. If I can ever get 
# rpy2 to work on my computer, perhaps these steps can be called directly from
# here in the future, but the future is not now. If you like to live life on the
# edge (and have R installed), you can just blindly run the line below.
# Otherwise, navigate to Main.R and step through that process.
import subprocess
subprocess.call("Rscript " + source + "/code/Main.R", shell=True)


##### Analysis summaries and plots #####
from polynomialPlots import *
polynomialExamplePlots(source + "/plots/")
polynomialPlots(data, 'Flight00001', 16.75, source + "/plots/") #True Positive
polynomialPlots(data, 'Flight00001', 3, source + "/plots/")     #True Negative
polynomialPlots(data, 'Flight00001', 37.5, source + "/plots/")  #False Positive
polynomialPlots(data, 'Flight00009', 32, source + "/plots/")    #False Negative


##### Create test data #####
from generateTestData import generateTestData
dataTest = generateTestData()
dataTest.to_csv(source + "/dataTest/dataTest.csv", index=False)

# Summary of base data
from baseDataStatistics import baseDataStatistics
baseDataStatistics(dataTest, source + "/dataTest/")


##### Run models against test data #####



##### END OF FILE #####
