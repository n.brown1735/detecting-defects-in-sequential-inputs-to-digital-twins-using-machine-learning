"""
kalmanFilterTest.py

Notes:
    The purpose of this module is to test the Kalman filter module using an
    example with expected values for each output.
    Source: https://www.kalmanfilter.net/multiExamples.html

Use:
    See each function below

"""

from numpy import array, eye, linalg, allclose

# Function to initialize Kalman filter
def initKalman():
    '''
        The purpose of this function is to initialize the Kalman filter test.
        
        Outputs:
            x       -- Initial assumed state
            P       -- Initial assumed estimate uncertainty
    '''
    x = array([[  0,   0,   0,   0,   0,   0]]).T
    P = array([[500,   0,   0,   0,   0,   0],
               [  0, 500,   0,   0,   0,   0],
               [  0,   0, 500,   0,   0,   0],
               [  0,   0,   0, 500,   0,   0],
               [  0,   0,   0,   0, 500,   0],
               [  0,   0,   0,   0,   0, 500]])
                
    return x, P
                
    
# Function to step through measurements and update the Kalman filter
def stepKalman(x, P, deltaT, sigma_a, sigma_m, z):
    '''
        Inputs:
            x       -- Current state
            P       -- Current estimate uncertainty
            deltaT  -- Time difference since current state
            sigma_a -- Acceleration st dev
            sigma_m -- Measurement st dev
            z       -- Measurement
            
        Outputs:
            x       -- Next state
            P       -- Next estimate uncertainty
            
    '''
    
    # State transition matrix
    F = array([[1, deltaT, 0.5*(deltaT**2), 0,      0,               0],
               [0,      1,          deltaT, 0,      0,               0],
               [0,      0,               1, 0,      0,               0],
               [0,      0,               0, 1, deltaT, 0.5*(deltaT**2)],
               [0,      0,               0, 0,      1,          deltaT],
               [0,      0,               0, 0,      0,               1]])
                
    # Process noise matrix
    Q = array([[(deltaT**4)/4, (deltaT**3)/2, (deltaT**2)/2, 0, 0, 0],
               [(deltaT**3)/2,     deltaT**2,        deltaT, 0, 0, 0],
               [(deltaT**2)/2,        deltaT,             1, 0, 0, 0],
               [0, 0, 0, (deltaT**4)/4, (deltaT**3)/2, (deltaT**2)/2],
               [0, 0, 0, (deltaT**3)/2,     deltaT**2,        deltaT],
               [0, 0, 0, (deltaT**2)/2,        deltaT,             1]])
    Q = Q * sigma_a**2
    
    # Observation matrix
    H = array([[1, 0, 0, 0, 0, 0],
               [0, 0, 0, 1, 0, 0]])
                
    # Measurement covariance (uncertainty) matrix
    R = array([[sigma_m**2,          0],
               [         0, sigma_m**2]])
                
    # Identity matrix the size and shape of F
    I = array(eye(F.shape[0]))
    
    # Predict the next state and covariance
    x = F @ x
    P = F @ P @ F.T + Q
    
    # Kalman gain
    K = P @ H.T @ linalg.inv(H @ P @ H.T + R)

    # State update
    x = x + K @ (z - H @ x)
    
    # Covariance update
    P = (I - K @ H) @ P @ (I - K @ H).T + K @ R @ K.T
    
    return x, P
    


# Function to predict the next step based on current Kalman state without updates
def predictKalman(x, P, deltaT, sigma_a):
    '''
        Inputs:
            x       -- Current state
            P       -- Current estimate uncertainty
            deltaT  -- Time difference since current state
            sigma_a -- Accleration st dev
            
        Outputs:
            predx   -- Predicted next state
            predP   -- Predicted next estimate uncertainty
            
    '''
    
    # State transition matrix
    F = array([[1, deltaT, 0.5*(deltaT**2), 0,      0,               0],
               [0,      1,          deltaT, 0,      0,               0],
               [0,      0,               1, 0,      0,               0],
               [0,      0,               0, 1, deltaT, 0.5*(deltaT**2)],
               [0,      0,               0, 0,      1,          deltaT],
               [0,      0,               0, 0,      0,               1]])
    
    # Process noise matrix
    Q = array([[(deltaT**4)/4, (deltaT**3)/2, (deltaT**2)/2, 0, 0, 0],
               [(deltaT**3)/2,     deltaT**2,        deltaT, 0, 0, 0],
               [(deltaT**2)/2,        deltaT,             1, 0, 0, 0],
               [0, 0, 0, (deltaT**4)/4, (deltaT**3)/2, (deltaT**2)/2],
               [0, 0, 0, (deltaT**3)/2,     deltaT**2,        deltaT],
               [0, 0, 0, (deltaT**2)/2,        deltaT,             1]])
    Q = Q * sigma_a**2
    
    # Predict next state and covariance
    predx = F @ x
    predP = F @ P @ F.T + Q
    
    return predx, predP


def main():
    '''
        Notes:
            Runs all tests.
            
        Source:
            https://www.kalmanfilter.net/multiExamples.html
    '''
    
    # Initialize example values
    deltaT = 1
    sigma_a = 0.2
    sigma_m = 3
    
    # Initialize state and covariance
    x, P = initKalman()
    
    # Take first measurement and compare output to expectations
    z = array([[301.5, -401.46]]).T
    x, P = stepKalman(x, P, deltaT, sigma_a, sigma_m, z)
    
    if allclose(x, array([[ 299.10716396],[ 199.40832091],[ 66.47298525],
                          [-398.27383762],[-265.52061199],[-88.5115909 ]])):
        print('First step passed.')
    else:
        print('First step failed.')

    # Take second measurement and compare output to expectations
    z = array([[298.23, -375.44]]).T
    x, P = stepKalman(x, P, deltaT, sigma_a, sigma_m, z)
    
    if allclose(x, array([[ 300.37082534],[ -28.2255886 ],[ -66.52999653],
                          [-378.48922238],[  64.87040914],[ 100.92734259]])):
        print('Second step passed.')
    else:
        print('Second step failed.')
        
    # Final prediction step and compare to expectations
    x, P = predictKalman(x, P, deltaT, sigma_a)
    
    if allclose(x, array([[ 238.88023847],[ -94.75558513],[ -66.52999653],
                          [-263.15514194],[ 165.79775173],[ 100.92734259]])):
        print('Prediction step passed.')
    else:
        print('Prediction step failed.')

# Run it!
main()
