"""
testRepeatableRandom.py

Notes:
    The purpose of this module is to run a test of the repeatability of
    repeatableRandom.py and generateData.py. The repeatable random numbers
    should be identical across different systems, OS, and architectures. The
    comparison takes advantage of the SHA256 algorithm to compare hashes rather
    than storing a very long string in plaintext in this file or elsewhere.

Use:
    Run the contents of this module. It will produce a result of either PASS or
    FAIL. Ideally, it will PASS on all machines...
  
"""

# Set path to code and data
source="C:/Users/nbrow/Dropbox/Myself/School/CSU/school/Dissertation/"
source.rstrip('//') # Remove final / if it exists

# Add src to path
import sys
sys.path.append(source + "/code/")
from repeatableRandom import *
from hashlib import sha256
from generateData import generateData

# Reset the generator
repRandReset()

# Build string of random numbers
thisStr = ''
for i in range(100):
    thisStr = thisStr + str(repRand())

# Compare to hash of random numbers generated on my machine
thisHash = sha256(thisStr.encode()).hexdigest()
myHash = '7e435587cfd931c70f9c5e9566c1179fe3b61f537712ff2b8e41cf6ae2374457'

# Verify that the hashes are equal
if myHash == thisHash:
    print("************************************************")
    print("Random stream test PASSED: Hashes are identical.")
    print("************************************************ \n")
else:
    print("Random stream test FAILED: The hashes are not identical \n")
    print("Original hash")
    print(str(myHash) + '\n')
    print("Generated hash")
    print(str(thisHash) + '\n')
    print("Confirm you are using numpy 1.24.3")

# Delete data
del thisStr
