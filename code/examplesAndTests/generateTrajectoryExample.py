"""
generateTrajectoryExample.py

Notes:
    The purpose of this module is to generate an example trajectory using
    rocketpy. The example is almost verbatim from the rocketpy documentation.

Use:
    Run module standalone
  
"""

from rocketpy import Environment, Rocket, SolidMotor, Flight

Env = Environment(
    railLength=5.2,
    latitude=32.990254,
    longitude=-106.974998,
    elevation=1400,
    date=(2023, 1, 22, 12)
) 

Env.setAtmosphericModel(type='StandardAtmosphere', file='GFS')

Pro75M1670 = SolidMotor(
    thrustSource="./my_env/Lib/data/motors/Cesaroni_M1670.eng",
    burnOut=3.9,
    grainNumber=5,
    grainSeparation=5/1000,
    grainDensity=1815,
    grainOuterRadius=33/1000,
    grainInitialInnerRadius=15/1000,
    grainInitialHeight=120/1000,
    nozzleRadius=33/1000,
    throatRadius=11/1000,
    interpolationMethod='linear'
)

Calisto = Rocket(
    motor=Pro75M1670,
    radius=127/2000,
    mass=19.197-2.956,
    inertiaI=6.60,
    inertiaZ=0.0351,
    distanceRocketNozzle=-1.255,
    distanceRocketPropellant=-0.85704,
    powerOffDrag='./my_env/Lib/data/calisto/powerOffDragCurve.csv',
    powerOnDrag='./my_env/Lib/data/calisto/powerOnDragCurve.csv'
)

Calisto.setRailButtons([0.2, -0.5])

NoseCone = Calisto.addNose(length=0.55829, kind="vonKarman", distanceToCM=0.71971)

FinSet = Calisto.addFins(4, span=0.100, rootChord=0.120, tipChord=0.040, distanceToCM=-1.04956)

Tail = Calisto.addTail(topRadius=0.0635, bottomRadius=0.0435, length=0.060, distanceToCM=-1.194656)

TestFlight = Flight(rocket=Calisto, environment=Env, inclination=85, heading=90)

#TestFlight.info()

TestFlight.allInfo()
