"""
polynomialPlots.py

Notes: TODO
    The purpose of this module is to provide plots for the polynomial regression
    defect detection method, including examples.

Use:
    polynomialPlots(data, flightID, lastTime, path)
        
        Arguments:
        data     -- Pandas DataFrame object containing flight data
        flightID -- Chosen flightID to plot
        lastTime -- Flight time to plot polynomial defect detection results
        path     -- (Optional) Path to location to save plots. If blank, do not save.
        
    polynomialExamplePlots(path)
        
        Arguments:
        path     -- (Optional) Path to location to save plots. If blank, do not save.

    Returns plots and saves them

"""

import matplotlib.pyplot as plt
import os

def polynomialExamplePlots(path=False):
    from itertools import chain
    from numpy import polyfit, poly1d
    
    # Setup theoretical flight path
    time = list(chain(range(1,51)))
    altitude = [(500*x-0.5*9.81*x**2) for x in time]
    
    # Plot example path
    plt.scatter(time, altitude, s=10, color='blue')
    plt.xlabel('Time (s)')
    plt.ylabel('Altitude (m)')
    plt.title('Polynomial Regression Defect Example')
    trend = polyfit(time, altitude, 9)
    trendpoly = poly1d(trend)
    plt.plot(time, trendpoly(time), color='blue')
    
    # Plot polynomial regression predicted path and two possible defects
    time.append(51)
    altitude.append((500*time[50]-0.5*9.81*time[50]**2)) #Predicted
    plt.plot(time[50]+1, altitude[50], marker="$✓$", color='green')
    time.append(51)
    altitude.append(altitude[50]+1000) #Defect
    plt.plot(time[50]+1, altitude[51], marker="$X$", color='red')
    time.append(51)
    altitude.append(altitude[50]-1000) #Defect
    plt.plot(time[50]+1, altitude[52], marker="$X$", color='red')
    plt.scatter(time, altitude, s=10, color='blue')
    
    
    # Plot polynomial regression confidence bounds (exagerated for demonstration)
    x = [time[50]-1, time[50]+1]
    plt.plot(x, [altitude[50]+500, altitude[50]+500], linestyle='dotted', color='red')
    plt.plot(x, [altitude[50]-500, altitude[50]-500], linestyle='dotted', color='red')
    
    plt.show()
        
    # Save plot, if path is provided
    if os.path.exists(path):
        plt.savefig(path + 'ExamplePolynomial.png', bbox_inches = "tight", dpi=300)
        print("Saved images to " + path)
        
    # Reset plot    
    plt.close()
    
    
def polynomialPlots(data, flight, lastTime, path=False):
    from numpy import polyfit, poly1d
    from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
    
    window = 10 #Window for polynomial regression models
    
    # Setup flight path
    tempdf = data[(data.flightID==flight) & (data.time<=lastTime)].tail(window + 1)
    
    if (len(tempdf) != window + 1):
        print("Not enough time intervals to perform polynomial regression. No plots saved.")
        return
    
    fig, ax = plt.subplots()
    
    # Plot flight path
    plt.scatter(tempdf.time, tempdf.z, s=10, color='blue')
    plt.xlabel('Time (s)')
    plt.ylabel('Altitude (m)')
    plt.title('Polynomial Regression Defect Detection Result')
    trend = polyfit(tempdf.time, tempdf.zClean, 2)
    trendpoly = poly1d(trend)
    plt.plot(tempdf.time, trendpoly(tempdf.time), color='blue')
    
    
    # Plot polynomial regression confidence bounds
    x = [tempdf.time.iloc[10]-0.1, tempdf.time.iloc[10]+0.1]
    plt.plot(x, [tempdf.polyZupper.iloc[10], tempdf.polyZupper.iloc[10]], linestyle='dotted', color='red')
    plt.plot(x, [tempdf.polyZlower.iloc[10], tempdf.polyZlower.iloc[10]], linestyle='dotted', color='red')
    
    # Usually the difference is very small, zoom in
    [pltYmin, pltYmax] = plt.gca().get_ylim()
    ylimMin = min(tempdf.polyZlower.iloc[10], tempdf.z.iloc[10]) #*0.9999
    ylimMax = max(tempdf.polyZupper.iloc[10], tempdf.z.iloc[10]) #*1.0001
    
    heightRatio = 0.33
    zoom = heightRatio*(pltYmax-pltYmin)/(ylimMax-ylimMin)
    zax = zoomed_inset_axes(ax, zoom, loc=2)
    zax.set_ylim((ylimMin, ylimMax))
    zax.set_yticks([])
    
    widthRatio = 0.5
    [pltXmin, pltXmax] = plt.gca().get_xlim()
    width = widthRatio*(pltXmax-pltXmin)/zoom
    zax.set_xlim((lastTime-width, lastTime+width))
    zax.set_xticks([])
    
    
    # Plot everything again in the zoomed in plot
    zax.scatter(tempdf.time, tempdf.z, s=10, color='blue')
    zax.plot(tempdf.time, trendpoly(tempdf.time), color='blue')
    zax.plot(x, [tempdf.polyZupper.iloc[10], tempdf.polyZupper.iloc[10]], linestyle='dotted', color='red')
    zax.plot(x, [tempdf.polyZlower.iloc[10], tempdf.polyZlower.iloc[10]], linestyle='dotted', color='red')

    
    plt.show()
        
    # Save plot, if path is provided
    if os.path.exists(path):
        plt.savefig(path + 'Polynomial_' + flight + '_Time_' + str(lastTime) + '.png'
                    , bbox_inches = "tight", dpi=300)
        print("Saved image to " + path)
        
    # Reset plot    
    plt.close()
