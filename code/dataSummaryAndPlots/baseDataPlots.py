"""
baseDataPlots.py

Notes: TODO
    The purpose of this module is to provide plots on the base data before
    defect detection.

Use:
    flightPlots(data, flightID, path)
        
        Arguments:
        data     -- Pandas DataFrame object containing flight data
        flightID -- Chosen flightID to plot
        path     -- (Optional) Path to location to save plots. If blank, do not save.
        
    defectExamplePlots(path)
        
        Arguments:
        path     -- (Optional) Path to location to save plots. If blank, do not save.

    Returns plots and saves them

"""

import matplotlib.pyplot as plt
import os

def flightPlots(data, flight, path=False):
    # Generate altitude plot over time
    plt.scatter(data.loc[data.flightID == flight].time
              , data.loc[data.flightID == flight].z
              , s=5, color='blue')
    plt.xlabel('Time (s)')
    plt.ylabel('Altitude (m)')
    plt.title('Altitude of ' + flight)
    plt.show()
    # Save plot, if path is provided
    if os.path.exists(path):
        plt.savefig(path + flight + '_Altitude.png', bbox_inches = "tight", dpi=300)
        print("Saved images to " + path)
        
    # Reset plot    
    plt.close()
    
    # Generate X plot over time
    plt.scatter(data.loc[data.flightID == flight].time
              , data.loc[data.flightID == flight].x
              , s=5, color='blue')
    plt.xlabel('Time (s)')
    plt.ylabel('X Direction (m)')
    plt.title('X Direction of ' + flight)
    plt.show()
    # Save plot, if path is provided
    if os.path.exists(path):
        plt.savefig(path + flight + '_X.png', bbox_inches = "tight", dpi=300)
        print("Saved images to " + path)
        
    # Reset plot    
    plt.close()
    
    # Generate Y plot over time
    plt.scatter(data.loc[data.flightID == flight].time
              , data.loc[data.flightID == flight].y
              , s=5, color='blue')
    plt.xlabel('Time (s)')
    plt.ylabel('Y Direction (m)')
    plt.title('Y Direction of ' + flight)
    plt.show()
    # Save plot, if path is provided
    if os.path.exists(path):
        plt.savefig(path + flight + '_Y.png', bbox_inches = "tight", dpi=300)
        print("Saved images to " + path)
        
    # Reset plot    
    plt.close()
    
    
def defectExamplePlots(path=False):
    from itertools import chain
    from numpy import polyfit, poly1d
    
    # Setup theoretical flight path
    time = list(chain(range(1,51)))
    altitude = [(500*x-0.5*9.81*x**2) for x in time]
    
    # Modify last value
    altitude[49]=altitude[49]+500
    
    # Plot example path
    plt.scatter(time, altitude, s=10, color='blue')
    plt.xlabel('Time (s)')
    plt.ylabel('Altitude (m)')
    plt.title('Example Trajectory')
    plt.axis([0, 55, 0, 15000])
    plt.show()
    # Save plot, if path is provided
    if os.path.exists(path):
        plt.savefig(path + 'Example.png', bbox_inches = "tight", dpi=300)
        print("Saved images to " + path)
        
    # Plot "good" data point
    time.append(51)
    altitude.append(altitude[49]+500)
    trend = polyfit(time, altitude, 9)
    trendpoly = poly1d(trend)
    plt.plot(time, trendpoly(time), color='blue')
    plt.plot(time[50]+1, altitude[50]+200, marker="$?$", color='black')
    plt.axis([0, 55, 0, 15000])
    plt.show()
        
    # Save plot, if path is provided
    if os.path.exists(path):
        plt.savefig(path + 'ExampleGood.png', bbox_inches = "tight", dpi=300)
        print("Saved images to " + path)
        
    # Reset plot    
    plt.close()
    
    
    #####
    # Plot example again, for likely "defective" data point
    del time[-1]
    del altitude[-1]
    plt.xlabel('Time (s)')
    plt.ylabel('Altitude (m)')
    plt.title('Example Trajectory')
    
    time.append(51)
    altitude.append((500*time[50]-0.5*9.81*time[50]**2))
    plt.scatter(time, altitude, s=10, color='blue')
    trend = polyfit(time, altitude, 2)
    trendpoly = poly1d(trend)
    plt.plot(time, trendpoly(time), color='blue')
    plt.plot(time[49], altitude[49], marker="$X$", color='red')
    plt.plot(time[50]+1, altitude[50], marker="$?$", color='black')
    plt.axis([0, 55, 0, 15000])
    plt.show()
    
    
    # Save plot, if path is provided
    if os.path.exists(path):
        plt.savefig(path + 'ExampleDefect.png', bbox_inches = "tight", dpi=300)
        print("Saved images to " + path)
    
    # Reset plot    
    plt.close()
    
