"""
kalmanFilterPlots.py

Notes: TODO
    The purpose of this module is to demonstrate the Kalman Filter and its
    capabilities.

Use:
    kalmanFilterPlots(data, flightID, path)
        
        Arguments:
        data     -- Pandas DataFrame object containing flight data
        flightID -- Chosen flightID to plot
        path     -- (Optional) Path to location to save plots. If blank, do not save.

    Returns plots and saves them

"""

import matplotlib.pyplot


def kalmanFilterPlots(data, flightID, path=False):
    
    # TODO check that dataFrame contains Kalman filter data, check that flightID exists or throw error
    
    # TODO plot stuff
    
    # TODO save plots, if path != 'n/a'
