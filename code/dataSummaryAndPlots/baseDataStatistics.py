"""
baseDataStatistics.py

Notes: TODO
    The purpose of this module is to provide statistics on the base data before
    defect detection.

Use:
    baseDataStatistics(data)
        
        Arguments:
        data     -- Pandas DataFrame object containing flight data
        path     -- Path to location to save plots. If blank, do not save.

    Returns statistics and saves them

"""

def baseDataStatistics(data, path=False):
    import pandas
    # Validate path
    import os  
    if not os.path.exists(path):
        print("Path of the file is Invalid")
        import sys
        sys.exit()
    
    # Flight statistics (altitude, distance travelled, time, etc.)
    flightStats = data.groupby('flightID').agg({'time': lambda x: max(x, key=abs)})
    flightStats['xMax'] = data.groupby('flightID').agg({'xClean': lambda x: max(x, key=abs)})
    flightStats['yMax'] = data.groupby('flightID').agg({'yClean': lambda x: max(x, key=abs)})
    flightStats['zMax'] = data.groupby('flightID').agg({'zClean': lambda x: max(x, key=abs)})
    flightStats['flightDistance'] = (flightStats.xMax**2 + flightStats.yMax**2)**0.5
    
    summaryStats = pandas.DataFrame()
    summaryStats['attribute'] = ['Time', 'Altitude', 'FlightDistance']
    summaryStats['min'] = [flightStats.time.min(), flightStats.zMax.min(), flightStats.flightDistance.min()]
    summaryStats['max'] = [flightStats.time.max(), flightStats.zMax.max(), flightStats.flightDistance.max()]
    summaryStats['mean'] = [flightStats.time.mean(), flightStats.zMax.mean(), flightStats.flightDistance.mean()]
    summaryStats['median'] = [flightStats.time.median(), flightStats.zMax.median(), flightStats.flightDistance.median()]
    
    # Row defects per flight, defect severity
    defectsPerFlight = data.groupby('flightID')['rowDefect'].sum()
    defectSeverity = data.groupby('defectSeverity')['rowDefect'].size()
    
    # Save data
    flightStats.reset_index().to_csv(path + "flightStatistics.csv", index=False)
    summaryStats.to_csv(path + "flightSummaryStats.csv", index=False)
    defectsPerFlight.reset_index().to_csv(path + "defectsPerFlight.csv", index=False)
    defectSeverity.reset_index().to_csv(path + "defectSeverity.csv", index=False)
    print("Saved data to " + path)
